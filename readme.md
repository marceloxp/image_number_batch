# Image Number Batch
> Batch one image to many (same) images with rendered number.

Installation: `npm install image_number_batch -g`

### Example usage:
> Goto image folder in command prompt
```
C:\..\..\images>image_numbers
-----------------------------------------------------------------
Image Number Batch
Process started
-----------------------------------------------------------------
Image filename: leviatan.png
Base name: output
Color Base [B]lack | [W]hite: W
Font size [8,16,32,64,128]: 64
Start: 1
End: 3
-----------------------------------------------------------------
Image loaded
write output-1.jpg
write output-2.jpg
write output-3.jpg
test process 1 3
test process 2 3
test process 3 3
-----------------------------------------------------------------
Process completed!
```