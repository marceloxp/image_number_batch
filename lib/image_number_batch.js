var Jimp     = require('jimp');
var readline = require('readline');
var path     = require('path');

Object.size = function(obj) { var size = 0, key; for (key in obj) { if (obj.hasOwnProperty(key)) size++; } return size; };

console.log('-----------------------------------------------------------------');
console.log('Image Number Batch');
console.log('Process started');
console.log('-----------------------------------------------------------------');

var params = {};
var fonts =
{
	'loaded': false
};

function loadFonts(p_font_size, p_callback)
{
	Jimp
		.loadFont( getFontBySize(p_font_size, 'WHITE') )
		.then
		(
			function(font)
			{
				fonts.white = font;
				Jimp
					.loadFont( getFontBySize(p_font_size, 'BLACK') )
					.then
					(
						function(font)
						{
							fonts.black = font;
							fonts.loaded = true;
							p_callback();
						}
					)
				;
			}
		)
	;
}

function makeQuestions()
{
	var keys = Object.keys(questions);
	var key = keys[a_length];
	var question = questions[key];
	rl.question
	(
		question.caption + ': ',
		function(answer)
		{
			answers[key] = answer;
			a_length = Object.size(answers);
			if (a_length >= q_length)
			{
				startProcessImages(answers);
			}
			else
			{
				makeQuestions();
			}
		}
	);
	rl.write(question.default);
}

function startProcessImages(p_args)
{
	params = p_args;
	params.total = (params.end - params.ini) + 1;
	loadFonts
	(
		params.fontsize,
		function()
		{
			processImages();
		}
	);
}

function getFontBySize(p_size, p_color)
{
	var color = p_color || 'BLACK';
	color = color.toUpperCase();
	var result;

	switch(color)
	{
		case 'WHITE':
			result = Jimp.FONT_SANS_64_WHITE;
			switch (parseInt(p_size))
			{
				case 8:
					result = Jimp.FONT_SANS_8_WHITE;
				break;
				case 16:
					result = Jimp.FONT_SANS_16_WHITE;
				break;
				case 32:
					result = Jimp.FONT_SANS_32_WHITE;
				break;
				case 64:
					result = Jimp.FONT_SANS_64_WHITE;
				break;
				case 128:
					result = Jimp.FONT_SANS_128_WHITE;
				break;
			}
		break;
		default:
			result = Jimp.FONT_SANS_64_BLACK;
			switch (parseInt(p_size))
			{
				case 8:
					result = Jimp.FONT_SANS_8_BLACK;
				break;
				case 16:
					result = Jimp.FONT_SANS_16_BLACK;
				break;
				case 32:
					result = Jimp.FONT_SANS_32_BLACK;
				break;
				case 64:
					result = Jimp.FONT_SANS_64_BLACK;
				break;
				case 128:
					result = Jimp.FONT_SANS_128_BLACK;
				break;
			}
		break;
	}
	return result;
}

function processImages()
{
	console.log('-----------------------------------------------------------------');
	var ext = path.extname(params.filename);
	var colors = [fonts.black, fonts.white];
	if ( (params.basecolor.toUpperCase() === 'B') || (params.basecolor.toUpperCase() === 'BLACK') )
	{
		colors = [fonts.white, fonts.black];
	}

	Jimp.read(params.filename)
		.then
		(
			function(image)
			{
				console.log('Image loaded');
				var index;
				for(var k = params.ini; k <= params.end; k++)
				{
					index = k.toString();
					console.log('write ' + params.basename + '-' + index + '.jpg');
					image
						.clone()
						.print(colors[0], 12, 12, index)
						.print(colors[1], 10, 10, index)
						.write(params.basename + '-' + index + ext, exitProcess)
					;
				}
			}
		)
		.catch
		(
			function(err)
			{
				console.log(err);
				process.exit(0);
			}
		)
	;
}

function exitProcess()
{
	writes++;
	console.log('test process', writes, params.total);
	if (writes >= params.total)
	{
		console.log('-----------------------------------------------------------------');
		console.log('Process completed!');
		process.exit(0);
	}
}

var rl = readline.createInterface(process.stdin, process.stdout);
var writes = 0;
var answers = {};
var a_length = Object.size(answers);

var questions =
{
	'filename' : { 'caption': 'Image filename'              , 'default': ''       },
	'basename' : { 'caption': 'Base name'                   , 'default': 'output' },
	'basecolor': { 'caption': 'Color Base [B]lack | [W]hite', 'default': 'W'      },
	'fontsize' : { 'caption': 'Font size [8,16,32,64,128]'  , 'default': '64'     },
	'ini'      : { 'caption': 'Start'                       , 'default': '1'      },
	'end'      : { 'caption': 'End'                         , 'default': '3'      }
};
var q_length = Object.size(questions);


function execute_module()
{
    makeQuestions();
}

module.exports = execute_module;